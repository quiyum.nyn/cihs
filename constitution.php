<?php
require_once "vendor/autoload.php";
include("templateLayout/templateInformation.php");

?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<head>
    <title><?php echo $title;?></title>
    <?php include("templateLayout/css/meta.php");?>
    <?php include("templateLayout/css/templateCss.php");?>
<style>

</style>
</head>

<body class="home-page">
<div class="wrapper">
    <!-- ******HEADER****** -->
    <?php include("templateLayout/headerAndNavigation.php");?>
    <!-- ******CONTENT****** -->
    <div class="content container">
        <div class="page-wrapper">
            <header class="page-heading clearfix">
                <h1 class="heading-title pull-left">Constitution</h1>
                <div class="breadcrumbs pull-right">
                    <ul class="breadcrumbs-list">
                        <li class="breadcrumbs-label">You are here:</li>
                        <li><a href="index.php">Home</a><i class="fa fa-angle-right"></i></li>
                        <li class="current">Constitution</li>
                    </ul>
                </div><!--//breadcrumbs-->
            </header>
            <div class="page-content">
                <div class="row page-row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <h4 class="text-center">গঠনতন্ত্র</h4>
                            <a href="resources/faruk.pdf" class="btn btn-primary" target="_blank" download><i class="fa fa-download" aria-hidden="true"></i>Click to download</a>
                            <object data="resources/faruk.pdf" type="application/pdf" width="100%" height="500px">
                                <p>It appears you don't have a PDF plugin for this browser.
                                    No biggie... you can <a href="resources/faruk.pdf">click here to
                                        download the PDF file.</a></p>
                            </object>
                       </div>

                </div>
                </div><!--//page-row-->
            </div><!--//page-content-->
        </div><!--//page-->
    </div><!--//content-->
</div><!--//wrapper-->

<!-- ******FOOTER****** -->
<?php include("templateLayout/footer.php");?>


<?php include("templateLayout/script/templateScript.php");?>

</body>
</html>

