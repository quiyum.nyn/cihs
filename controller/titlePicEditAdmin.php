<?php
require_once("../vendor/autoload.php");
use App\Message\Message;
use App\Utility\Utility;
use App\News;
$object=new News();

if(isset($_FILES['title_pic']['name']))
{
    $picName=time().$_FILES['title_pic']['name'];
    $tmp_name=$_FILES['title_pic']['tmp_name'];
    $data = getimagesize($tmp_name);
    $width=$data[0];
    $height = $data[1];
    if($width==300 && $height==300){
        move_uploaded_file($tmp_name,'../resources/news/title/'.$picName);
        $_POST['title_pic']=$picName;
        $object->prepareData($_POST);
        $object->updateTitleAdmin();
        Message::setMessage("Picture update successfully!");
        return Utility::redirect($_SERVER['HTTP_REFERER']);
    }
    else{
        Message::setMessage("Picture size doesn't match!");
        return Utility::redirect($_SERVER['HTTP_REFERER']);
    }
}
else{
    Message::setMessage("Add picture!");
    return Utility::redirect($_SERVER['HTTP_REFERER']);
}