<?php
require_once("../vendor/autoload.php");
use App\Members;
use App\Message\Message;
use App\Utility\Utility;
$object= new Members();
$object->prepareData($_GET);
$object->approveMember();
Message::setMessage("Member request has been approved!");
Utility::redirect('../admin/member_requests.php');
?>