<?php
require_once("../vendor/autoload.php");
use App\Members;
use App\Message\Message;
use App\Utility\Utility;
$object= new Members();
$object->prepareData($_GET);
$object->rejectMember();
Message::setMessage("Member request has been ignored!");
Utility::redirect('../admin/member_requests.php');
?>