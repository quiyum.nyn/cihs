<?php
require_once("../vendor/autoload.php");
use App\Message\Message;
use App\Utility\Utility;
use App\News;
$object=new News();
$object->prepareData($_GET);
$object->restore();
Message::setMessage("New has been restore!");
Utility::redirect('../admin/news.php');