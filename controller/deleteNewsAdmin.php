<?php
require_once("../vendor/autoload.php");
use App\Message\Message;
use App\Utility\Utility;
use App\News;
$object=new News();
$object->prepareData($_GET);
$object->delete();
Message::setMessage("New has been deleted!");
Utility::redirect('../admin/deleted_news.php');