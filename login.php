<?php
require_once('vendor/autoload.php');
include("templateLayout/templateInformation.php");
use App\Message\Message;
session_start();
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<head>
    <title><?php echo $title;?></title>
    <?php include("templateLayout/css/meta.php");?>
    <?php include("templateLayout/css/templateCss.php");?>
</head>

<body class="home-page">
<div class="wrapper">
    <!-- ******HEADER****** -->
    <?php include("templateLayout/headerAndNavigation.php");?>

    <!-- ******CONTENT****** -->
    <div class="content container">
        <div class="page-wrapper">
            <header class="page-heading clearfix">
                <h1 class="heading-title pull-left">Admin Login</h1>
                <div class="breadcrumbs pull-right">
                    <ul class="breadcrumbs-list">
                        <li class="breadcrumbs-label">You are here:</li>
                        <li><a href="index.php">Home</a><i class="fa fa-angle-right"></i></li>
                        <li class="current">Admin login</li>
                    </ul>
                </div><!--//breadcrumbs-->
            </header>
            <div class="page-content">
                <div class="row">
                    <article class="contact-form col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3 col-xs-10 col-xs-offset-1  page-row">
                        <?php
                        if(isset($_SESSION) && !empty($_SESSION['message'])) {

                            $msg = Message::getMessage();

                            echo "
                        <p id='message' style='text-align: center; font-family:Century Gothic;color: red;font-size: 14px;font-weight: 600;'>$msg</p>";

                        }

                        ?>
                        <h3 class="title">Login Yourself.</h3>
                        <form action="<?php echo base_url;?>controller/processLogin.php" method="post">
                            <div class="form-group">
                                <label>E-mail</label>
                                <input type="email" class="form-control" name="email" placeholder="type your email here" required>
                            </div><!--//form-group-->
                            <div class="form-group">
                                <label>password<span class="required">*</span></label>
                                <input type="password" class="form-control" name="password" placeholder="type your password here" required>
                            </div><!--//form-group-->
                            <button type="submit" class="btn btn-theme">Log In</button>
                        </form>
                    </article><!--//contact-form-->
                </div><!--//page-row-->
            </div><!--//page-content-->
        </div><!--//page-wrapper-->
    </div><!--//content-->
</div><!--//wrapper-->

<?php include("templateLayout/footer.php");?>
<!-- Javascript -->
<?php include("templateLayout/script/templateScript.php");?>
<script>
    function mydate1()
    {
        d=new Date(document.getElementById("dt").value);
        dt=d.getDate();
        mn=d.getMonth();
        mn++;
        yy=d.getFullYear();
        document.getElementById("ndt").value=dt+"/"+mn+"/"+yy
        document.getElementById("ndt").hidden=false;
        document.getElementById("dt").hidden=true;
    }
</script>
</body>
</html>

