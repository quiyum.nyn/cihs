<?php
$base_url=base_url;
$index=$base_url.'index.php';
$registration=$base_url.'registration.php';
$news=$base_url.'news.php';
$gallery=$base_url.'gallery.php';
$about=$base_url.'about.php';
$patrons=$base_url.'patrons.php';
$member=$base_url.'members.php';
$contact=$base_url.'contact.php';
$event=$base_url.'events.php';
$memberProfile=$base_url.'memberProfile.php';
$admin=$base_url.'login.php';
$poll=$base_url.'poll.php';
$poll2=$base_url.'pollDetails.php';
$poll3=$base_url.'closedPoll.php';
$assoc=$base_url.'adviser&executive.php';
$assoc2=$base_url.'members.php';


$url ='http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'];
if($url==$index){
    $indexClass="active";
}
elseif($url==$registration){
    $regClass="active";
}
elseif($url==$registration){
    $regClass="active";
}
elseif($url==$news){
    $newsClass="active";
}
elseif($url==$poll){
    $pollClass="active";
}
elseif($url==$poll2){
    $pollClass="active";
}
elseif($url==$poll3){
    $pollClass="active";
}
elseif($url==$assoc){
    $assocClass="active";
}
elseif($url==$assoc2){
    $assocClass="active";
}
elseif($url==$gallery){
    $galleyClass="active";
}
elseif($url==$patrons){
    $patronClass="active";
}
elseif($url==$member){
    $memberClass="active";
}
elseif($url==$contact){
    $contactClass="active";
}
elseif($url==$about){
    $aboutClass="active";
}
elseif($url==$memberProfile){
    $memberClass="active";
}
elseif($url==$event){
    $eventClass="active";
}
elseif($url==$admin){
    $adminClass="active";
}
?>
<header class="header">
    <div class="top-bar">
        <div class="container">
            <ul class="social-icons col-md-6 col-sm-6 col-xs-12 hidden-xs">
                <li><a href="#" ><i class="fa fa-twitter"></i></a></li>
                <li><a href="https://www.facebook.com/tarunno.arl.pg/" target="_blank" ><i class="fa fa-facebook"></i></a></li>
                <li><a href="#" ><i class="fa fa-youtube"></i></a></li>
                <li><a href="#" ><i class="fa fa-google-plus"></i></a></li>
                <li class="row-end"><a href="#" ><i class="fa fa-rss"></i></a></li>
            </ul><!--//social-icons-->
            <form class="pull-right search-form" role="search">
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Search the site...">
                </div>
                <button type="submit" class="btn btn-theme">Go</button>
            </form>
        </div>
    </div><!--//to-bar-->
    <div class="header-main container">
        <h1 class="logo col-md-4 col-sm-4">
            <a href="<?php echo base_url;?>index.php"><img id="logo" src="<?php echo base_url;?>resources/assets/images/logo.png" alt="Logo"></a>
        </h1><!--//logo-->
        <div class="info col-md-8 col-sm-8">
            <ul class="menu-top navbar-right hidden-xs">
                <li class="divider"><a href="<?php echo base_url;?>index.php">Home</a></li>
                <li><a href="<?php echo base_url;?>contact.php">Contact</a></li>
            </ul><!--//menu-top-->


        </div><!--//info-->
    </div><!--//header-main-->
</header><!--//header-->

<!-- ******NAV****** -->
<nav class="main-nav" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button><!--//nav-toggle-->
        </div><!--//navbar-header-->

        <div class="navbar-collapse collapse" id="navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="nav-item <?php echo $indexClass;?>"><a href="<?php echo base_url;?>index.php">Home</a></li>
                <li class="nav-item dropdown <?php echo $assocClass;?>"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Who We Are <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="<?php echo base_url;?>constitution.php"">Constitution</a></li>
                        <li><a href="<?php echo base_url;?>committee.php"">The Committees</a></li>
                        <li><a href="<?php echo base_url;?>members.php"">Members</a></li>
                    </ul>
                </li>
                <li class="nav-item <?php echo $eventClass;?>"><a href="<?php echo base_url;?>events.php">Events</a></li>
                <li class="nav-item <?php echo $newsClass;?>"><a href="<?php echo base_url;?>news.php">News</a></li>
                <li class="nav-item <?php echo $galleyClass;?>"><a href="<?php echo base_url;?>gallery.php">Gallery</a></li>
                <li class="nav-item <?php echo $regClass;?>"><a href="<?php echo base_url;?>registration.php">Registration</a></li>
                <li class="nav-item <?php echo $contactClass;?>"><a href="<?php echo base_url;?>contact.php">Contact</a></li>
                <li class="nav-item <?php echo $adminClass;?>"><a href="<?php echo base_url;?>login.php">Admin Login</a></li>
            </ul><!--//nav-->
        </div><!--//navabr-collapse-->
    </div><!--//container-->
</nav><!--//main-nav-->
