
<!-- Javascript -->
<script type="text/javascript" src="<?php echo base_url;?>resources/assets/plugins/jquery-1.11.2.min.js"></script>

<script type="text/javascript" src="<?php echo base_url;?>resources/assets/plugins/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url;?>resources/assets/plugins/bootstrap.min.js"></script>

<script type="text/javascript" src="<?php echo base_url;?>resources/assets/plugins/bootstrap-hover-dropdown.min.js"></script>
<script type="text/javascript" src="<?php echo base_url;?>resources/assets/plugins/back-to-top.js"></script>
<script type="text/javascript" src="<?php echo base_url;?>resources/assets/plugins/jquery-placeholder/jquery.placeholder.js"></script>
<script type="text/javascript" src="<?php echo base_url;?>resources/assets/plugins/pretty-photo/js/jquery.prettyPhoto.js"></script>
<script type="text/javascript" src="<?php echo base_url;?>resources/assets/plugins/flexslider/jquery.flexslider-min.js"></script>
<script type="text/javascript" src="<?php echo base_url;?>resources/assets/plugins/jflickrfeed/jflickrfeed.min.js"></script>
<script type="text/javascript" src="<?php echo base_url;?>resources/assets/js/main.js"></script>
<script type="text/javascript" src="<?php echo base_url;?>resources/assets/js/jspdf.min.js"></script>
<script type="text/javascript" src="<?php echo base_url;?>resources/assets/js/html2canvas.min.js"></script>
<script type="text/javascript" src="<?php echo base_url;?>resources/assets/js/html2pdf.js"></script>
<script>
    jQuery(function($) {
        $('#message').fadeOut (1550);
        $('#message').fadeIn (1550);
        $('#message').fadeOut (50);

    })
</script>