<?php
require_once "vendor/autoload.php";
include("templateLayout/templateInformation.php");

?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<head>
    <title><?php echo $title;?></title>
    <?php include("templateLayout/css/meta.php");?>
    <?php include("templateLayout/css/templateCss.php");?>

</head>

<body class="home-page">
<div class="wrapper">
    <!-- ******HEADER****** -->
    <?php include("templateLayout/headerAndNavigation.php");?>
    <!-- ******CONTENT****** -->
    <div class="content container">
        <div class="page-wrapper">
            <header class="page-heading clearfix">
                <h1 class="heading-title pull-left">Committee</h1>
                <div class="breadcrumbs pull-right">
                    <ul class="breadcrumbs-list">
                        <li class="breadcrumbs-label">You are here:</li>
                        <li><a href="index.php">Home</a><i class="fa fa-angle-right"></i></li>
                        <li class="current">Committee</li>
                    </ul>
                </div><!--//breadcrumbs-->
            </header>
            <div class="page-content">
                <div class="row page-row">
                    <div class="news-wrapper col-md-12 col-sm-12 col-xs-12">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <h4 class="text-center">উপদেষ্টা পরিষদ</h4>
                                <div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-12 col-xs-offset-0">
                                    <div class="col-md-3 col-sm-3 col-xs-6">
                                        <div class="thumbnail">
                                            <div style="min-height:">
                                                <img src="resources/committee/mir_shakil.jpg" class="img-responsive img-rounded" >
                                            </div>
                                        </div>
                                        <p class=" text-center">মীর সোহরাব শাকিল</p>
                                        <p class=" text-center">প্রধান উপদেষ্টা</p>
                                        <p class=" text-center">২০০২ ব্যাচ</p>
                                    </div>
                                    <div class="col-md-3 col-sm-3 col-xs-6">
                                        <div class="thumbnail">
                                            <div style="min-height:">
                                                <img src="resources/committee/female.png" class="img-responsive img-rounded" >
                                            </div>
                                        </div>
                                        <p class=" text-center">সালেহা আনোয়ার </p>
                                        <p class=" text-center"> উপদেষ্টা</p>
                                        <p class=" text-center">২০০২ ব্যাচ</p>
                                    </div>
                                    <div class="col-md-3 col-sm-3 col-xs-6">
                                        <div class="thumbnail">
                                            <div style="min-height:">
                                                <img src="resources/committee/farhad.jpg" class="img-responsive img-rounded" >
                                            </div>
                                        </div>
                                        <p class=" text-center">ফরহাদ উদ্দীন </p>
                                        <p class=" text-center"> উপদেষ্টা</p>
                                        <p class=" text-center">২০০২ ব্যাচ</p>
                                    </div>
                                    <div class="col-md-3 col-sm-3 col-xs-6">
                                        <div class="thumbnail">
                                            <div style="min-height:">
                                                <img src="resources/committee/hasan.jpg" class="img-responsive img-rounded" >
                                            </div>
                                        </div>
                                        <p class=" text-center">হাসান চৌধুরী </p>
                                        <p class=" text-center"> উপদেষ্টা</p>
                                        <p class=" text-center">২০০২ ব্যাচ</p>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-12 col-xs-offset-0">
                                            <div class="col-md-4 col-sm-4 col-xs-6 ">
                                                <div class="thumbnail">
                                                    <div style="min-height:">
                                                        <img src="resources/committee/female.png" class="img-responsive img-rounded" >
                                                    </div>
                                                </div>
                                                <p class=" text-center">জেসমিন আক্তার </p>
                                                <p class=" text-center"> উপদেষ্টা</p>
                                                <p class=" text-center">২০০২ ব্যাচ</p>
                                            </div>
                                            <div class="col-md-4 col-sm-4 col-xs-6">
                                                <div class="thumbnail">
                                                    <div style="min-height:">
                                                        <img src="resources/committee/female.png" class="img-responsive img-rounded" >
                                                    </div>
                                                </div>
                                                <p class=" text-center">ইফফাত রহমান </p>
                                                <p class=" text-center"> উপদেষ্টা</p>
                                                <p class=" text-center">২০০৪ ব্যাচ</p>
                                            </div>
                                            <div class="col-md-4 col-sm-4 col-xs-6">
                                                <div class="thumbnail">
                                                    <div style="min-height:">
                                                        <img src="resources/committee/pic.jpg" class="img-responsive img-rounded" >
                                                    </div>
                                                </div>
                                                <p class=" text-center">রবিউল আলম তৌহিদ </p>
                                                <p class=" text-center"> উপদেষ্টা</p>
                                                <p class=" text-center">২০০৫ ব্যাচ</p>
                                            </div>
                                        </div>
                                    </div>

                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12">
                           <hr>
                            <h4 class="text-center">কার্যকরী পরিষদ</h4>
                            <div class="col-md-2 col-sm-2 col-xs-6">
                                <div class="thumbnail">
                                    <div style="min-height:">
                                        <img src="resources/committee/titu.jpg" class="img-responsive img-rounded" >
                                    </div>
                                </div>
                                <p class=" text-center">সাইফুল্লাহ খালেদ টিটু </p>
                                <p class=" text-center">সভাপতি</p>
                                <p class=" text-center">২০০৩ ব্যাচ</p>
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-6">
                                <div class="thumbnail">
                                    <div style="min-height:">
                                        <img src="resources/committee/pic.jpg" class="img-responsive img-rounded" >
                                    </div>
                                </div>
                                <p class=" text-center">মনিরুল ইসলাম </p>
                                <p class=" text-center">সহ-সভাপতি</p>
                                <p class=" text-center">২০০৪ ব্যাচ</p>
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-6">
                                <div class="thumbnail">
                                    <div style="min-height:">
                                        <img src="resources/committee/asad.jpg" class="img-responsive img-rounded" >
                                    </div>
                                </div>
                                <p class=" text-center">আসাদুল আমীন </p>
                                <p class=" text-center">সহ-সভাপতি</p>
                                <p class=" text-center">২০০৪ ব্যাচ</p>
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-6">
                                <div class="thumbnail">
                                    <div style="min-height:">
                                        <img src="resources/committee/osman.jpg" class="img-responsive img-rounded" >
                                    </div>
                                </div>
                                <p class=" text-center">মোঃ ওসমান </p>
                                <p class=" text-center">সহ-সভাপতি</p>
                                <p class=" text-center">২০০৫ ব্যাচ</p>
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-6">
                                <div class="thumbnail">
                                    <div style="min-height:">
                                        <img src="resources/committee/pic.jpg" class="img-responsive img-rounded" >
                                    </div>
                                </div>
                                <p class=" text-center">ইয়াসিন রাসেল </p>
                                <p class=" text-center">সহ-সভাপতি</p>
                                <p class=" text-center">২০০৭ ব্যাচ</p>
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-6">
                                <div class="thumbnail">
                                    <div style="min-height:">
                                        <img src="resources/committee/miku.jpg" class="img-responsive img-rounded" >
                                    </div>
                                </div>
                                <p class=" text-center">শওকত আলী মিকু </p>
                                <p class=" text-center">সহ-সভাপতি</p>
                                <p class=" text-center">২০০৮ ব্যাচ</p>
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-6">
                                <div class="thumbnail">
                                    <div style="min-height:">
                                        <img src="resources/committee/pic.jpg" class="img-responsive img-rounded" >
                                    </div>
                                </div>
                                <p class=" text-center">উমর ফারুক </p>
                                <p class=" text-center">সাধারণ সম্পাদক</p>
                                <p class=" text-center">২০০৬ ব্যাচ</p>
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-6">
                                <div class="thumbnail">
                                    <div style="min-height:">
                                        <img src="resources/committee/pic.jpg" class="img-responsive img-rounded" >
                                    </div>
                                </div>
                                <p class=" text-center">খালেদ মাহমুদ নাসিফ </p>
                                <p class=" text-center">যুগ্ম সাধারণ সম্পাদক</p>
                                <p class=" text-center">২০০৬ ব্যাচ</p>
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-6">
                                <div class="thumbnail">
                                    <div style="min-height:">
                                        <img src="resources/committee/pic.jpg" class="img-responsive img-rounded" >
                                    </div>
                                </div>
                                <p class=" text-center">সাখাওয়াত হোসেন </p>
                                <p class=" text-center">যুগ্ম সাধারণ সম্পাদক</p>
                                <p class=" text-center">২০০৬ ব্যাচ</p>
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-6">
                                <div class="thumbnail">
                                    <div style="min-height:">
                                        <img src="resources/committee/pic.jpg" class="img-responsive img-rounded" >
                                    </div>
                                </div>
                                <p class=" text-center">রাজিবুল হক </p>
                                <p class=" text-center">যুগ্ম সাধারণ সম্পাদক</p>
                                <p class=" text-center">২০০৭ ব্যাচ</p>
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-6">
                                <div class="thumbnail">
                                    <div style="min-height:">
                                        <img src="resources/committee/pic.jpg" class="img-responsive img-rounded" >
                                    </div>
                                </div>
                                <p class=" text-center">সাজ্জাদুর রহমান </p>
                                <p class=" text-center">যুগ্ম সাধারণ সম্পাদক</p>
                                <p class=" text-center">২০০৮ ব্যাচ</p>
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-6">
                                <div class="thumbnail">
                                    <div style="min-height:">
                                        <img src="resources/committee/pic.jpg" class="img-responsive img-rounded" >
                                    </div>
                                </div>
                                <p class=" text-center">আব্দুন নুর</p>
                                <p class=" text-center">সাংগঠনিক সম্পাদক</p>
                                <p class=" text-center">২০০৯ ব্যাচ</p>
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-6">
                                <div class="thumbnail">
                                    <div style="min-height:">
                                        <img src="resources/committee/pic.jpg" class="img-responsive img-rounded" >
                                    </div>
                                </div>
                                <p class=" text-center">মোঃ নাঈম </p>
                                <p class=" text-center">সাংগঠনিক সম্পাদক</p>
                                <p class=" text-center">২০০৭ ব্যাচ</p>
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-6">
                                <div class="thumbnail">
                                    <div style="min-height:">
                                        <img src="resources/committee/pic.jpg" class="img-responsive img-rounded" >
                                    </div>
                                </div>
                                <p class=" text-center">হাবিবুর রহমান রিপন </p>
                                <p class=" text-center">সাংগঠনিক সম্পাদক</p>
                                <p class=" text-center">২০১০ ব্যাচ</p>
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-6">
                                <div class="thumbnail">
                                    <div style="min-height:">
                                        <img src="resources/committee/pic.jpg" class="img-responsive img-rounded" >
                                    </div>
                                </div>
                                <p class=" text-center">তৌফিকুর রহমান </p>
                                <p class=" text-center">অর্থ সম্পাদক</p>
                                <p class=" text-center">২০০৪ ব্যাচ</p>
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-6">
                                <div class="thumbnail">
                                    <div style="min-height:">
                                        <img src="resources/committee/pic.jpg" class="img-responsive img-rounded" >
                                    </div>
                                </div>
                                <p class=" text-center">জয়নাল আবেদীন </p>
                                <p class=" text-center">সহ-অর্থ সম্পাদক</p>
                                <p class=" text-center">২০০৬ ব্যাচ</p>
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-6">
                                <div class="thumbnail">
                                    <div style="min-height:">
                                        <img src="resources/committee/pic.jpg" class="img-responsive img-rounded" >
                                    </div>
                                </div>
                                <p class=" text-center">মোঃ দস্তগীর </p>
                                <p class=" text-center">সহ-অর্থ সম্পাদক</p>
                                <p class=" text-center">২০০৯ ব্যাচ</p>
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-6">
                                <div class="thumbnail">
                                    <div style="min-height:">
                                        <img src="resources/committee/pic.jpg" class="img-responsive img-rounded" >
                                    </div>
                                </div>
                                <p class=" text-center">মঞ্জুরুল আলম </p>
                                <p class=" text-center">প্রচার ও প্রকাশনা সম্পাদক</p>
                                <p class=" text-center">২০০৬ ব্যাচ</p>
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-6">
                                <div class="thumbnail">
                                    <div style="min-height:">
                                        <img src="resources/committee/pic.jpg" class="img-responsive img-rounded" >
                                    </div>
                                </div>
                                <p class=" text-center">মোঃ জোবায়ের আকিব </p>
                                <p class=" text-center">সহ-প্রচার ও প্রকাশনা</p>
                                <p class=" text-center">২০০৮ ব্যাচ</p>
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-6">
                                <div class="thumbnail">
                                    <div style="min-height:">
                                        <img src="resources/committee/pic.jpg" class="img-responsive img-rounded" >
                                    </div>
                                </div>
                                <p class=" text-center">আনিসুর রহমান </p>
                                <p class=" text-center">অফিস সম্পাদক</p>
                                <p class=" text-center">২০০৮ ব্যাচ</p>
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-6">
                                <div class="thumbnail">
                                    <div style="min-height:">
                                        <img src="resources/committee/pic.jpg" class="img-responsive img-rounded" >
                                    </div>
                                </div>
                                <p class=" text-center">মারসিয়াত ইভান</p>
                                <p class=" text-center">সহ-অফিস সম্পাদক</p>
                                <p class=" text-center">২০১০ ব্যাচ</p>
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-6">
                                <div class="thumbnail">
                                    <div style="min-height:">
                                        <img src="resources/committee/pic.jpg" class="img-responsive img-rounded" >
                                    </div>
                                </div>
                                <p class=" text-center">শাহাদাত হোসেন কানন </p>
                                <p class=" text-center">শিক্ষা বিষয়ক সম্পাদক</p>
                                <p class=" text-center">২০০৮ ব্যাচ</p>
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-6">
                                <div class="thumbnail">
                                    <div style="min-height:">
                                        <img src="resources/committee/pic.jpg" class="img-responsive img-rounded" >
                                    </div>
                                </div>
                                <p class=" text-center">মোঃ মঞ্জুর </p>
                                <p class=" text-center">সহ-শিক্ষা সম্পাদক</p>
                                <p class=" text-center">২০০৯ ব্যাচ</p>
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-6">
                                <div class="thumbnail">
                                    <div style="min-height:">
                                        <img src="resources/committee/pic.jpg" class="img-responsive img-rounded" >
                                    </div>
                                </div>
                                <p class=" text-center">মাহমুদুল হাসান রিগেল</p>
                                <p class=" text-center">সাহিত্য ও সংস্কৃতি সম্পাদক</p>
                                <p class=" text-center">২০০৭ ব্যাচ</p>
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-6">
                                <div class="thumbnail">
                                    <div style="min-height:">
                                        <img src="resources/committee/pic.jpg" class="img-responsive img-rounded" >
                                    </div>
                                </div>
                                <p class=" text-center">মীর আশরাফ শাকিব </p>
                                <p class=" text-center">সহ-সাহিত্য ও সংস্কৃতি</p>
                                <p class=" text-center">২০০৭ ব্যাচ</p>
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-6">
                                <div class="thumbnail">
                                    <div style="min-height:">
                                        <img src="resources/committee/pic.jpg" class="img-responsive img-rounded" >
                                    </div>
                                </div>
                                <p class=" text-center">যুলকফিল শাহ মাকতুম </p>
                                <p class=" text-center">ছাত্র কল্যাণ সম্পাদক</p>
                                <p class=" text-center">২০০৯ ব্যাচ</p>
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-6">
                                <div class="thumbnail">
                                    <div style="min-height:">
                                        <img src="resources/committee/pic.jpg" class="img-responsive img-rounded" >
                                    </div>
                                </div>
                                <p class=" text-center">নাজমুল কবির </p>
                                <p class=" text-center">সহ-ছাত্র কল্যান</p>
                                <p class=" text-center">২০০৯ ব্যাচ</p>
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-6">
                                <div class="thumbnail">
                                    <div style="min-height:">
                                        <img src="resources/committee/quiyum.jpg" class="img-responsive img-rounded" >
                                    </div>
                                </div>
                                <p class=" text-center">আবদুল্লাহ আল কাইয়ুম </p>
                                <p class=" text-center">বিজ্ঞান ও প্রযুক্তি সম্পাদক</p>
                                <p class=" text-center">২০০৮ ব্যাচ</p>
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-6">
                                <div class="thumbnail">
                                    <div style="min-height:">
                                        <img src="resources/committee/pic.jpg" class="img-responsive img-rounded" >
                                    </div>
                                </div>
                                <p class=" text-center">ইরফান উদ্দীন ইমন </p>
                                <p class=" text-center">সহ-বিজ্ঞান ও প্রযুক্তি</p>
                                <p class=" text-center">২০০৯ ব্যাচ</p>
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-6">
                                <div class="thumbnail">
                                    <div style="min-height:">
                                        <img src="resources/committee/pic.jpg" class="img-responsive img-rounded" >
                                    </div>
                                </div>
                                <p class=" text-center">ওমর সাদেক</p>
                                <p class=" text-center">পাঠাগার সম্পাদক</p>
                                <p class=" text-center">২০০৬ ব্যাচ</p>
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-6">
                                <div class="thumbnail">
                                    <div style="min-height:">
                                        <img src="resources/committee/minhaz.jpg" class="img-responsive img-rounded" >
                                    </div>
                                </div>
                                <p class=" text-center">মিনহাজ শুভ </p>
                                <p class=" text-center">সহ-পাঠাগার সম্পাদক</p>
                                <p class=" text-center">২০০৮ ব্যাচ</p>
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-6">
                                <div class="thumbnail">
                                    <div style="min-height:">
                                        <img src="resources/committee/pic.jpg" class="img-responsive img-rounded" >
                                    </div>
                                </div>
                                <p class=" text-center">শাহাদাত হোসেন শাহেদ </p>
                                <p class=" text-center">ক্রীড়া সম্পাদক</p>
                                <p class=" text-center">২০০৭ ব্যাচ</p>
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-6">
                                <div class="thumbnail">
                                    <div style="min-height:">
                                        <img src="resources/committee/pic.jpg" class="img-responsive img-rounded" >
                                    </div>
                                </div>
                                <p class=" text-center">আবুল কাশেম হিরু </p>
                                <p class=" text-center">সহ-ক্রীড়া সম্পাদক</p>
                                <p class=" text-center">২০০৯ ব্যাচ</p>
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-6">
                                <div class="thumbnail">
                                    <div style="min-height:">
                                        <img src="resources/committee/pic.jpg" class="img-responsive img-rounded" >
                                    </div>
                                </div>
                                <p class=" text-center">মামুনুর রশিদ</p>
                                <p class=" text-center">স্বাস্থ্য বিষয়ক সম্পাদক</p>
                                <p class=" text-center">২০০৭ ব্যাচ</p>
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-6">
                                <div class="thumbnail">
                                    <div style="min-height:">
                                        <img src="resources/committee/pic.jpg" class="img-responsive img-rounded" >
                                    </div>
                                </div>
                                <p class=" text-center">মোহাম্মদ আরিফ </p>
                                <p class=" text-center">সহ-স্বাস্থ্য বিষয়ক সম্পাদক</p>
                                <p class=" text-center">২০০২ ব্যাচ</p>
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-6">
                                <div class="thumbnail">
                                    <div style="min-height:">
                                        <img src="resources/committee/pic.jpg" class="img-responsive img-rounded" >
                                    </div>
                                </div>
                                <p class=" text-center">মোঃ মোবাসসির </p>
                                <p class=" text-center">সমাজসেবা বিষয়ক সম্পাদক</p>
                                <p class=" text-center">২০০৯ ব্যাচ</p>
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-6">
                                <div class="thumbnail">
                                    <div style="min-height:">
                                        <img src="resources/committee/pic.jpg" class="img-responsive img-rounded" >
                                    </div>
                                </div>
                                <p class=" text-center">আব্দুর রহিম  </p>
                                <p class=" text-center">সহ-সমাজসেবা সম্পাদক</p>
                                <p class=" text-center">২০১০ ব্যাচ</p>
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-6">
                                <div class="thumbnail">
                                    <div style="min-height:">
                                        <img src="resources/committee/pic.jpg" class="img-responsive img-rounded" >
                                    </div>
                                </div>
                                <p class=" text-center">তৌহদিুল ইসলাম মামুন </p>
                                <p class=" text-center">তথ্য-প্রযুক্তি বিষয়ক সম্পাদক</p>
                                <p class=" text-center">২০১০ ব্যাচ</p>
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-6">
                                <div class="thumbnail">
                                    <div style="min-height:">
                                        <img src="resources/committee/pic.jpg" class="img-responsive img-rounded" >
                                    </div>
                                </div>
                                <p class=" text-center">তাসনুভা রুবাইয়্যাত </p>
                                <p class=" text-center">মহিলা বিষয়ক সম্পাদিকা</p>
                                <p class=" text-center">২০০৫ ব্যাচ</p>
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-6">
                                <div class="thumbnail">
                                    <div style="min-height:">
                                        <img src="resources/committee/pic.jpg" class="img-responsive img-rounded" >
                                    </div>
                                </div>
                                <p class=" text-center">রিফাত নাসরিন </p>
                                <p class=" text-center">সহ-মহিলা বিষয়ক সম্পাদিকা</p>
                                <p class=" text-center">২০০৮ ব্যাচ</p>
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-6">
                                <div class="thumbnail">
                                    <div style="min-height:">
                                        <img src="resources/committee/pic.jpg" class="img-responsive img-rounded" >
                                    </div>
                                </div>
                                <p class=" text-center">তানজিনা জাহান </p>
                                <p class=" text-center">সহ-মহিলা বিষয়ক সম্পাদিকা</p>
                                <p class=" text-center">২০০৯ ব্যাচ</p>
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-6">
                                <div class="thumbnail">
                                    <div style="min-height:">
                                        <img src="resources/committee/pic.jpg" class="img-responsive img-rounded" >
                                    </div>
                                </div>
                                <p class=" text-center">রাশেদুল ইসলাম জীবন </p>
                                <p class=" text-center">নির্বাহী সদস্য</p>
                                <p class=" text-center">২০১১ ব্যাচ</p>
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-6">
                                <div class="thumbnail">
                                    <div style="min-height:">
                                        <img src="resources/committee/pic.jpg" class="img-responsive img-rounded" >
                                    </div>
                                </div>
                                <p class=" text-center">রাকিবুল ইসলাম রাকিব  </p>
                                <p class=" text-center">নির্বাহী সদস্য</p>
                                <p class=" text-center">২০১২ ব্যাচ</p>
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-6">
                                <div class="thumbnail">
                                    <div style="min-height:">
                                        <img src="resources/committee/pic.jpg" class="img-responsive img-rounded" >
                                    </div>
                                </div>
                                <p class=" text-center">সুলতান বায়েজীদ </p>
                                <p class=" text-center">নির্বাহী সদস্য</p>
                                <p class=" text-center">২০১৩ ব্যাচ</p>
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-6">
                                <div class="thumbnail">
                                    <div style="min-height:">
                                        <img src="resources/committee/pic.jpg" class="img-responsive img-rounded" >
                                    </div>
                                </div>
                                <p class=" text-center">শাফিন মাহমুদ </p>
                                <p class=" text-center">নির্বাহী সদস্য</p>
                                <p class=" text-center">২০১৪ ব্যাচ</p>
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-6">
                                <div class="thumbnail">
                                    <div style="min-height:">
                                        <img src="resources/committee/pic.jpg" class="img-responsive img-rounded" >
                                    </div>
                                </div>
                                <p class=" text-center">আমানুল্লাহ শিপন </p>
                                <p class=" text-center">নির্বাহী সদস্য</p>
                                <p class=" text-center">২০১৫ ব্যাচ</p>
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-6">
                                <div class="thumbnail">
                                    <div style="min-height:">
                                        <img src="resources/committee/pic.jpg" class="img-responsive img-rounded" >
                                    </div>
                                </div>
                                <p class=" text-center">আশরাফুল ইসলাম রবি </p>
                                <p class=" text-center">নির্বাহী সদস্য</p>
                                <p class=" text-center">২০১৬ ব্যাচ</p>
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-6">
                                <div class="thumbnail">
                                    <div style="min-height:">
                                        <img src="resources/committee/pic.jpg" class="img-responsive img-rounded" >
                                    </div>
                                </div>
                                <p class=" text-center">মোঃ ইরফান </p>
                                <p class=" text-center">নির্বাহী সদস্য</p>
                                <p class=" text-center">২০১৭ ব্যাচ</p>
                            </div>



                        </div>
                    </div><!--//news-wrapper-->

                </div><!--//page-row-->
            </div><!--//page-content-->
        </div><!--//page-->
    </div><!--//content-->
</div><!--//wrapper-->

<!-- ******FOOTER****** -->
<?php include("templateLayout/footer.php");?>


<?php include("templateLayout/script/templateScript.php");?>

</body>
</html>

