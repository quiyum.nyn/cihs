<?php
require_once("vendor/autoload.php");
include ("templateLayout/templateInformation.php");
session_start();
$object = new \App\Members();
$object->prepareData($_GET);
$oneData =$object->viewOne();
$date = date("d", strtotime("$oneData->birth_date"));
$month = date("m", strtotime("$oneData->birth_date"));
$year = date("Y", strtotime("$oneData->birth_date"));
if($month=='01'){
    $monthD='January';
}
if($month=='02'){
    $monthD='February';
}
else if($month=='03'){
    $monthD='March';
}
else if($month=='04'){
    $monthD='April';
}
else if($month=='05'){
    $monthD='May';
}
else if($month=='06'){
    $monthD='June';
}
else if($month=='07'){
    $monthD='July';
}
else if($month=='08'){
    $monthD='August';
}
else if($month=='09'){
    $monthD='September';
}
else if($month=='10'){
    $monthD='October';
}
else if($month=='11'){
    $monthD='November';
}
else if($month=='12'){
    $monthD='December';
}
$batchMate=explode(",",$oneData->batch_mate);
$batchMateContact=explode(",",$oneData->batch_mate_contact);
$batchMateFB=explode(",",$oneData->batch_mate_fb);
$batchMate1=$batchMate[0];
$batchMateContact1=$batchMateContact[0];
$batchMateFB1=$batchMateFB[0];
$batchMate2=$batchMate[1];
$batchMateContact2=$batchMateContact[1];
$batchMateFB2=$batchMateFB[1];

if($oneData->passing_year>=2002 && $oneData->passing_year<=2010){
    $passing_year=$oneData->passing_year;
}
else if($oneData->passing_year>=2011 && $oneData->passing_year<=2017){
    $passing_year=$oneData->passing_year;
}
else if($oneData->passing_year>=9999){
    $passing_year="Regular";
}
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<head>
    <title><?php echo $title;?></title>
    <?php include("templateLayout/css/meta.php");?>
    <?php include ("templateLayout/css/templateCss.php");?>
    <?php include('templateLayout/css/tableCss.php');?>
</head>

<body class="home-page">
<div class="wrapper">
    <!-- ******HEADER****** -->
    <?php include ("templateLayout/headerAndNavigation.php");?>

    <!-- ******CONTENT****** -->
    <div class="content container">
        <div class="page-wrapper">
            <header class="page-heading clearfix">
                <h1 class="heading-title pull-left">Member Profile</h1>
                <div class="breadcrumbs pull-right">
                    <ul class="breadcrumbs-list">
                        <li class="breadcrumbs-label">You are here:</li>
                        <li><a href="#">Home</a><i class="fa fa-angle-right"></i></li>
                        <li><a href="#">Admin</a><i class="fa fa-angle-right"></i></li>
                        <li class="current">Member Profile</li>
                    </ul>
                </div><!--//breadcrumbs-->
            </header>
        <div class="row">
            <div class="col-md-3 col-sm-3 col-xs-6 col-md-offset-2 col-sm-offset-2">
                <img src="resources/members_photo/<?php echo $oneData->picture?>" class="img-responsive img-rounded">

            </div>
            <div class="col-md-8 col-sm-8 col-xs-12 col-md-offset-2 col-sm-offset-2">
                <h5 style="color: #2e6da4"><b>Personal Information</b></h5>
                <div class="form-group name">
                    <label for="name">Name </label>
                    <input type="text" class="form-control" value="<?php echo $oneData->name?>" readonly>
                </div><!--//form-group-->
                <div class="form-group name">
                    <label for="name">Mother's Name </label>
                    <input type="text" class="form-control" value="<?php echo $oneData->mother_name?>" readonly>
                </div><!--//form-group-->
                <div class="form-group name">
                    <label for="name">Father's Name </label>
                    <input type="text" class="form-control" value="<?php echo $oneData->father_name?>" readonly>
                </div><!--//form-group-->
                <div class="form-group name">
                    <label for="name">Spouse Name </label>
                    <input type="text" class="form-control" value="<?php if($oneData->spouse_name==NULL){echo "N/A";} else{ echo $oneData->spouse_name;}?>" readonly>
                </div><!--//form-group-->
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="row">
                            <div class="col-md-12 cool-sm-12 col-xs-12">
                                <label for="name">Date of Birth</label>
                                <div class="row">
                                    <div class="col-md-4 col-sm-4 col-xs-4">
                                        <div class="form-group name">
                                            <input type="text" class="form-control" value="<?php echo $date?>" readonly>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-4 ">
                                        <div class="form-group name">
                                            <input type="text" class="form-control" value="<?php echo $monthD?>" readonly>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-4">
                                        <div class="form-group name">
                                            <input type="text" class="form-control" value="<?php echo $year?>" readonly>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group name">
                            <label for="name">Blood Group</label>
                            <input type="text" class="form-control" value="<?php echo $oneData->blood?>" readonly>
                        </div><!--//form-group-->

                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group name">
                            <label for="name">NID No</label>
                            <input type="text" class="form-control" value="<?php if($oneData->nid_no==NULL){echo "N/A";} else{ echo $oneData->nid_no;}?>" readonly>
                        </div><!--//form-group-->
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-6">
                        <div class="form-group name">
                            <label for="name">Gender</label>
                            <input type="text" class="form-control" value="<?php echo $oneData->gender?>" readonly>
                        </div><!--//form-group-->
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-6">
                        <div class="form-group name">
                            <label for="name">Marital Status</label>
                            <input type="text" class="form-control" value="<?php echo $oneData->marital_status?>" readonly>
                        </div><!--//form-group-->
                    </div>
                </div>

                <div class="form-group name">
                    <label for="name">Permanent Address</label>
                    <textarea class="form-control" readonly rows="2"><?php echo $oneData->permanent_adrs?></textarea>
                </div><!--//form-group-->
                <div class="form-group name">
                    <label for="name">Present Address</label>
                    <input type="text" class="form-control" value="<?php echo $oneData->present_adrs?>" readonly>
                </div><!--//form-group-->

                <div class="row">

                    <div class="col-md-4 col-sm-4 col-xs-6">
                        <div class="form-group name">
                            <label for="name">Mobile No</label>
                            <input type="text" class="form-control" value="<?php if($oneData->gender=='Female'){ echo "Not Shared";} else echo $oneData->contact?>" readonly>
                        </div><!--//form-group-->
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-6">
                        <div class="form-group name">
                            <label for="name">Email</label>
                            <input type="text" class="form-control" value="<?php echo $oneData->email?>" readonly>
                        </div><!--//form-group-->
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="form-group name">
                            <label for="name">FB ID</label>
                            <input type="text" class="form-control" value="<?php echo $oneData->fb_id?>" readonly>
                        </div><!--//form-group-->
                    </div>
                </div>

                <div class="row">

                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="form-group name">
                            <label for="name">Occupation</label>
                            <input type="text" class="form-control" value="<?php if($oneData->occupation==NULL){echo "N/A";} else{ echo $oneData->occupation;}?>" readonly>
                        </div><!--//form-group-->
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="form-group name">
                            <label for="name">Organization</label>
                            <input type="text" class="form-control" value="<?php if($oneData->organization==NULL){echo "N/A";} else{ echo $oneData->organization;}?>" readonly>
                        </div><!--//form-group-->
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="form-group name">
                            <label for="name">Designation</label>
                            <input type="text" class="form-control" value="<?php if($oneData->designation==NULL){echo "N/A";} else{ echo $oneData->designation;}?>" readonly>
                        </div><!--//form-group-->
                    </div>
                </div>
                <h5 style="color: #2e6da4"><br><br><b>School Information</b></h5>
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <div class="form-group name">
                            <label for="name">The class you admitted</label>
                            <input type="text" class="form-control" value="<?php echo $oneData->admitted_class?>" readonly>
                        </div><!--//form-group-->
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <div class="form-group name">
                            <label for="name">In which class you left</label>
                            <input type="text" class="form-control" value="<?php echo $oneData->left_class?>" readonly>
                        </div><!--//form-group-->
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group name">
                            <label for="name">Have you passed from the school?</label><br>
                            <input type="text" class="form-control" value="<?php echo $oneData->passed_from_school?>" readonly>
                        </div><!--//form-group-->
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group name">
                            <label for="name">Passing Year</label>
                            <input type="text" class="form-control" value="<?php echo $passing_year?>" readonly>
                        </div><!--//form-group-->
                    </div>
                </div>
                <h5 style="color: #2e6da4"><br><br><b>Batch Mate (For recognize)</b></h5>
                <div class="row">
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="form-group name">
                            <label for="name">Name (Person 1)</label>
                            <input type="text" class="form-control" value="<?php echo $batchMate1?>" readonly>
                        </div><!--//form-group-->
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-6">
                        <div class="form-group name">
                            <label for="name">Mobile No</label>
                            <input type="text" class="form-control" value="<?php echo $batchMateContact1?>" readonly>
                        </div><!--//form-group-->
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-6">
                        <div class="form-group name">
                            <label for="name">FB ID</label>
                            <input type="text" class="form-control" value="<?php echo $batchMateFB1?>" readonly>
                        </div><!--//form-group-->
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="form-group name">
                            <label for="name">Name (Person 2)</label>
                            <input type="text" class="form-control" value="<?php echo $batchMate2?>" readonly>
                        </div><!--//form-group-->
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-6">
                        <div class="form-group name">
                            <label for="name">Mobile No</label>
                            <input type="text" class="form-control" value="<?php echo $batchMateContact2?>" readonly>
                        </div><!--//form-group-->
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-6">
                        <div class="form-group name">
                            <label for="name">FB ID</label>
                            <input type="text" class="form-control" value="<?php echo $batchMateFB2?>" readonly>
                        </div><!--//form-group-->
                    </div>

                </div>
            </div>
        </div>
    </div>
    </div>
</div><!--//wrapper-->

<!-- ******FOOTER****** -->
<?php include ("templateLayout/footer.php");?>


<?php include ("templateLayout/script/templateScript.php");?>
<?php include('templateLayout/script/tableScript.php');?>
</body>
</html>

