<?php session_start();
require_once("vendor/autoload.php");
include("templateLayout/templateInformation.php");
use App\News;
$object=new News();
$topNews=$object->showTopNews();
$allNews=$object->showallNews();
$otherNews=$object->showOtherNews();
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->  
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->  
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->  
<head>
    <title><?php echo $title;?></title>
    <?php include("templateLayout/css/meta.php");?>
    <?php include("templateLayout/css/templateCss.php");?>
   
</head> 

<body class="home-page">
    <div class="wrapper">
        <!-- ******HEADER****** -->
        <?php include("templateLayout/headerAndNavigation.php");?>
        <!-- ******CONTENT****** --> 
        <div class="content container">
            <div id="promo-slider" class="slider flexslider">
                <ul class="slides">
                    <li>
                        <img src="resources/assets/images/slides/13.jpg" alt="" />
                        <p class="flex-caption">
                            <span class="main" > Chittagong Ideal High School Alumni Association</span>
                            <br />
                            <span class="secondary clearfix" >Since 2000 - One of Chittagong's top high schools.</span>
                        </p>
                    </li>
                    <li>
                        <img src="resources/assets/images/slides/13.jpg" alt="" />
                        <p class="flex-caption">
                            <span class="main" > Chittagong Ideal High School Alumni Association</span>
                            <br />
                            <span class="secondary clearfix" >Connect with your classmates.</span>
                        </p>
                    </li>
                    <li>
                        <img src="resources/assets/images/slides/13.jpg" alt="" />
                        <p class="flex-caption">
                            <span class="main" > Chittagong Ideal High School Alumni Association</span>
                            <br />
                            <span class="secondary clearfix" > Keeping our tradition alive.</span>
                        </p>
                    </li>
                    <li>
                        <img src="resources/assets/images/slides/13.jpg" alt="" />
                        <p class="flex-caption">
                            <span class="main" > Chittagong Ideal High School Alumni Association</span>
                            <br />
                            <span class="secondary clearfix" >Build a bright future.</span>
                        </p>
                    </li>
                </ul><!--//slides-->
            </div><!--//flexslider-->
            <section class="promo box box-dark">        
                <div class="col-md-9">
                <h4 class="section-heading">Online Membership</h4>
                    <p>Authority can take any decision on new membership .</p>
                </div>  
                <div class="col-md-3">
                    <a class="btn btn-cta" href="registration.php"><i class="fa fa-play-circle"></i>Apply Now</a>
                </div>
            </section><!--//promo-->
            <div class="row cols-wrapper">
                <div class="col-md-9">
                    <section class="links">
                        <h1 class="section-heading text-highlight"><span class="line">Latest News</span></h1>
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <?php
                                    foreach ($topNews as $top){
                                        $demonews=substr($top->news,0,1600);
                                        $demoHeadline=substr( $top->headline,0,500);
                                        $date = date("d", strtotime("$top->date"));
                                        $month = date("m", strtotime("$top->date"));
                                        $year = date("Y", strtotime("$top->date"));
                                        if($month=='01'){
                                            $monthD='January';
                                        }
                                        if($month=='02'){
                                            $monthD='February';
                                        }
                                        else if($month=='03'){
                                            $monthD='March';
                                        }
                                        else if($month=='04'){
                                            $monthD='April';
                                        }
                                        else if($month=='05'){
                                            $monthD='May';
                                        }
                                        else if($month=='06'){
                                            $monthD='June';
                                        }
                                        else if($month=='07'){
                                            $monthD='July';
                                        }
                                        else if($month=='08'){
                                            $monthD='August';
                                        }
                                        else if($month=='09'){
                                            $monthD='September';
                                        }
                                        else if($month=='10'){
                                            $monthD='October';
                                        }
                                        else if($month=='11'){
                                            $monthD='November';
                                        }
                                        else if($month=='12'){
                                            $monthD='December';
                                        }
                                        ?>
                                        <div class="col-md-4 col-sm-4 col-xs-12">
                                            <div class="item">
                                                <div class="news-item">
                                                    <img class="thumb" src="resources/news/title/<?php echo $top->title_pic?>" alt="" width="100%"/>
                                                    <h4 class="title"><a href="news_details.php?news_id=<?php echo $top->id?>"><br><?php echo $demoHeadline?></a></h4>
                                                    <p><?php echo $date; if($date==01){echo "<sup>st</sup>";} elseif($date==31){echo "<sup>st</sup>";} elseif($date==2){echo "<sup>nd</sup>";}elseif($date==3){echo "<sup>rd</sup>";} else{echo "<sup>th</sup>";} ?><?php echo " ".$monthD." ".$year?></p>
                                                    <p>Posted by: <a><?php echo $top->admin_name?></a></p>
                                                    <p><?php echo $demonews?></p>
                                                    <a class="read-more" href="news_details.php?news_id=<?php echo $top->id?>">Read more<i class="fa fa-chevron-right"></i></a>
                                                    <br>
                                                </div><!--//news-item-->

                                            </div>
                                        </div>
                                <?php
                                    }
                                ?>

                                <div class="col-md-8 col-sm-8 col-xs-12">
                                    <?php
                                    foreach ($allNews as $top){
                                        $date = date("d", strtotime("$top->date"));
                                        $month = date("m", strtotime("$top->date"));
                                        $year = date("Y", strtotime("$top->date"));
                                        if($month=='01'){
                                            $monthD='January';
                                        }
                                        if($month=='02'){
                                            $monthD='February';
                                        }
                                        else if($month=='03'){
                                            $monthD='March';
                                        }
                                        else if($month=='04'){
                                            $monthD='April';
                                        }
                                        else if($month=='05'){
                                            $monthD='May';
                                        }
                                        else if($month=='06'){
                                            $monthD='June';
                                        }
                                        else if($month=='07'){
                                            $monthD='July';
                                        }
                                        else if($month=='08'){
                                            $monthD='August';
                                        }
                                        else if($month=='09'){
                                            $monthD='September';
                                        }
                                        else if($month=='10'){
                                            $monthD='October';
                                        }
                                        else if($month=='11'){
                                            $monthD='November';
                                        }
                                        else if($month=='12'){
                                            $monthD='December';
                                        }
                                        $demoHeadline=substr( $top->headline,0,90);


                                        ?>
                                        <div class="col-md-4 col-sm-4 col-xs-6">
                                            <div class="item">
                                                <div class="news-item">
                                                    <img class="thumb" src="resources/news/title/<?php echo $top->title_pic?>" alt="" width="100%"/>
                                                    <div style="min-height: 50px">
                                                        <h5 class="title"><a href="news_details.php?news_id=<?php echo $top->id?>"><br><?php echo $demoHeadline?></a></h5>
                                                    </div>

                                                    <p><?php echo $date; if($date==01){echo "<sup>st</sup>";} elseif($date==31){echo "<sup>st</sup>";} elseif($date==2){echo "<sup>nd</sup>";}elseif($date==3){echo "<sup>rd</sup>";} else{echo "<sup>th</sup>";} ?><?php echo " ".$monthD." ".$year?></p>
                                                    <p>Posted by: <a><?php echo $top->admin_name?></a></p>
                                                    <a class="read-more" href="news_details.php?news_id=<?php echo $top->id?>">Read more<i class="fa fa-chevron-right"></i></a>
                                                    <br>
                                                </div><!--//news-item-->

                                            </div>
                                        </div>
                                        <?php
                                    }
                                    ?>

                                </div>

                            </div>
                        </div>
                    </section><!--//links-->


                </div>
                <div class="col-md-3">
                    <section class="testimonials">
                        <h1 class="section-heading text-highlight"><span class="line">শুভেচ্ছা বাণী</span></h1>
                        <div class="carousel-controls">
                            <a class="prev" href="#testimonials-carousel" data-slide="prev"><i class="fa fa-caret-left"></i></a>
                            <a class="next" href="#testimonials-carousel" data-slide="next"><i class="fa fa-caret-right"></i></a>
                        </div><!--//carousel-controls-->
                        <div class="section-content">
                            <div id="testimonials-carousel" class="testimonials-carousel carousel slide">
                                <div class="carousel-inner">
                                    <div class="item active">
                                        <blockquote class="quote">                                  
                                            <p><i class="fa fa-quote-left"></i>তারুণ্য শুধুই জীবনের একটি অংশ নয়। তারুণ্যের একটি ভিত্তি আছে। আর সেটি হচ্ছে আপনার মন, আপনার প্রতিভা,সৃজনশীলতা।
                                                যখন আপনি তারুণ্যের সন্ধান পাবেন, তখন আপনার কাছে আপনার বয়স পরাজিত হবে।
                                                তেমনি একঝাক তরুণদের সমন্বয়ে গড়া একটি সামাজিক ও উন্নয়নমূলক সংগঠন "তারুণ্য"। এ তরুনরাই একদিন আমাদের দেশের উন্নয়নের সোপান হবে।</p>
                                        </blockquote>                
                                        <div class="row">
                                            <p class="people col-md-8 col-sm-3 col-xs-8"><span class="name">সাইফুল্লাহ খালেদ টিটু</span><br /><span class="title"> সভাপতি <br> চিটাগাং আইডিয়্যাল হাই স্কুল অ্যালামনাই এসোসিয়েশন</span></p>
                                            <img class="profile col-md-4 col-sm-4 col-xs-4 pull-right img-responsive" src="resources/assets/images/testimonials/profile-1.png" alt="" />
                                        </div>                               
                                    </div><!--//item-->
                                    <div class="item">
                                        <blockquote class="quote">
                                            <p><i class="fa fa-quote-left"></i>
                                                তরুণদের জয়গান পৃথিবীতে চিরকাল ছিল ও থাকবে। তরুণদের মধ্যে ভিন্নভাবে চিন্তা করার ও উদ্ভাবনের সাহস থাকতে হবে।
                                                তরুণরা এগিয়ে যাক, এগিয়ে নিয়ে যাক পৃথিবীকে। "সুশিক্ষিত তারুণ্য, স্বনির্ভর বাংলাদেশ" এ শ্লোগান নিয়ে প্রায় এক যুগ আগে সামাজিক সংগঠন "তারুণ্য"-এর পথচলা।
                                                সংগঠন হিসেবে তারুণ্যের সফলতা কামনা করছি।</p>
                                        </blockquote>
                                        <div class="row">
                                            <p class="people col-md-8 col-sm-3 col-xs-8"><span class="name">ওমর ফারুখ</span><br /><span class="title"> সাধারন সম্পাদক <br> চিটাগাং আইডিয়্যাল হাই স্কুল অ্যালামনাই এসোসিয়েশন</span></p>
                                            <img class="profile col-md-4 col-sm-4 col-xs-4 pull-right img-responsive" src="resources/assets/images/testimonials/profile-2.png" alt="" />
                                        </div>                 
                                    </div><!--//item-->
                                   
                                    
                                </div><!--//carousel-inner-->
                            </div><!--//testimonials-carousel-->
                        </div><!--//section-content-->
                    </section><!--//testimonials-->
                    <section class="links" style="min-height: 202px">
                        <h1 class="section-heading text-highlight"><span class="line">Facebook Page</span></h1>
                        <div class="section-content">
                            <iframe src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2FCIHSAA%2F&width=300&layout=standard&action=like&size=small&show_faces=true&share=true&height=80&appId" width="100%" height="80" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
                        </div><!--//section-content-->
                    </section><!--//links-->

                </div><!--//col-md-3-->
            </div><!--//cols-wrapper-->

        </div><!--//content-->
    </div><!--//wrapper-->
    
    <!-- ******FOOTER****** -->
    <?php include("templateLayout/footer.php");?>


    <?php include("templateLayout/script/templateScript.php");?>
</body>
</html> 

