<?php
session_start();
include_once "vendor/autoload.php";
use App\Members;
$object=new Members();
$oneData=$object->view($_SESSION['last_id']);
if($oneData->passing_year>=2002 && $oneData->passing_year<=2010){
    $passing_year=$oneData->passing_year;
    $amount=1000;
}
else if($oneData->passing_year>=2011 && $oneData->passing_year<=2017){
    $amount=500;
    $passing_year=$oneData->passing_year;
}
else if($oneData->passing_year>=9999){
    $amount=200;
    $passing_year="Regular";
}
$date=$oneData->registration_date;
$new=date('Y-m-d H:i:s',strtotime('+72 hour +0 minutes',strtotime($date)));
$newdate=date('d/m/Y h:i:s a', strtotime($new));
$date = date("d", strtotime("$oneData->birth_date"));
$month = date("m", strtotime("$oneData->birth_date"));
$year = date("Y", strtotime("$oneData->birth_date"));
if($month=='01'){
    $monthD='January';
}
if($month=='02'){
    $monthD='February';
}
else if($month=='03'){
    $monthD='March';
}
else if($month=='04'){
    $monthD='April';
}
else if($month=='05'){
    $monthD='May';
}
else if($month=='06'){
    $monthD='June';
}
else if($month=='07'){
    $monthD='July';
}
else if($month=='08'){
    $monthD='August';
}
else if($month=='09'){
    $monthD='September';
}
else if($month=='10'){
    $monthD='October';
}
else if($month=='11'){
    $monthD='November';
}
else if($month=='12'){
    $monthD='December';
}

?>
<?php
include("templateLayout/templateInformation.php");
include_once "vendor/autoload.php";
use App\Message\Message;
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<head>
    <title><?php echo $title;?></title>
    <?php include("templateLayout/css/meta.php");?>
    <?php include("templateLayout/css/templateCss.php");?>

</head>

<body class="home-page">
<div class="wrapper">
    <!-- ******HEADER****** -->
    <?php include("templateLayout/headerAndNavigation.php");?>
    <!-- ******CONTENT****** -->
    <div class="content container">
        <div class="page-wrapper">
            <header class="page-heading clearfix">
                <h1 class="heading-title pull-left">Registration-Step2</h1>
                <div class="breadcrumbs pull-right">
                    <ul class="breadcrumbs-list">
                        <li class="breadcrumbs-label">You are here:</li>
                        <li><a href="index.php">Home</a><i class="fa fa-angle-right"></i></li>
                        <li class="current">Registration-Step2</li>
                    </ul>
                </div><!--//breadcrumbs-->
            </header>
            <div class="page-content">
                <div class="row">
                    <?php
                    if(isset($_SESSION) && !empty($_SESSION['message'])) {

                        $msg = Message::getMessage();

                        echo "
                        <p id='message' style='text-align: center; font-family:Century Gothic;color: red;font-size: 14px;font-weight: 600;'>$msg</p>";

                    }

                    ?>
                    <div class="col-md-8 col-sm-8 col-md-offset-2 col-sm-offset-2 col-xs-12">
                        <div class="stepwizard col-md-offset-3 col-sm-offset-3 col-xs-offset-3">
                            <div class="stepwizard-row setup-panel">
                                <div class="stepwizard-step">
                                    <a href="#" type="button" class="btn btn-info btn-circle" style="background: #3a3194;border: #3a3194;" disabled="disabled">1</a>
                                    <p>Step 1</p>
                                </div>
                                <div class="stepwizard-step">
                                    <a href="#" type="button" class="btn btn-warning btn-circle" style="background: #080058;border: #080058;">2</a>
                                    <p>Step 2</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <center><a href="pdf2.php" class="btn btn-primary"><i class="fa fa-download" aria-hidden="true"></i>Download Your Copy</a></center>
                        <br><br>
                    </div>

                        <article class="contact-form col-md-12 col-sm-12 col-xs-12  page-row">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading">  <h4 >
                                        </h4></div>
                                    <div class="panel-body">
                                        <div class="col-md-8 col-sm-8 col-xs-12 col-md-offset-2 col-sm-offset-2 ">
                                            <img alt="User Pic" src="resources/banner.jpg?>" id="profile-image1" class="img-responsive">
                                            <hr>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12 row">
                                            <div class="row">
                                                <div class="col-md-6 col-xs-8 col-sm-6 col-md-offset-1 col-sm-offset-1 col-xs-offset-2">
                                                    <h3><?php echo $oneData->name?></h3>
                                                    <p>+88-<?php echo $oneData->contact?></p>
                                                </div>
                                            </div>

                                            <hr>
                                            <div class="col-md-4 col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-0 col-md-offset-0">
                                                <img alt="User Pic" src="resources/members_photo/<?php echo $oneData->picture?>" id="profile-image1" class="img-rounded img-responsive">
                                                <br><br>
                                            </div>
                                            <div class="col-md-8 col-xs-12 col-xs-offset-0 col-sm-6 col-sm-offset-0 col-md-offset-0" >

                                                <table class="table table-responsive table-bordered" style="font-size: 14px;width: 100% ">
                                                    <tbody>
                                                    <tr>
                                                        <td>Date of Birth</td>
                                                        <td class="text-right"><?php echo $date;if($date==01){echo "<sup>st</sup>";} elseif($date==31){echo "<sup>st</sup>";} elseif($date==2){echo "<sup>nd</sup>";}elseif($date==3){echo "<sup>rd</sup>";} else{echo "<sup>th</sup>";}?> <?php echo " ".$monthD." ".$year?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Permanent Address</td>
                                                        <td class="text-right"><?php echo $oneData->permanent_adrs?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Passing Year</td>
                                                        <td class="text-right"><?php echo $passing_year?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Payable Amount</td>
                                                        <td class="text-right"><?php echo $amount?></td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="col-md-8 col-sm-8 col-xs-8 col-md-offset-2 col-sm-offset-2 col-xs-offset-2">
                                    <img src="resources/mobile-Banking-Billboard.jpg" class="img-responsive img-rounded">
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <h3 style="font-family: 'Agency FB';text-align: center">You can pay your registration fee with ROCKET</h3>
                                    <h4 style="font-family: 'Agency FB';text-align: center">Please follow these instruction</h4>
                                    <hr>
                                    <h5>
                                        <ol style="line-height: 22px;font-family: 'Agency FB';font-size: 16px">
                                            <li>Dial *322# from your mobile handset</li>
                                            <li>Press 1 for payment option.</li>
                                            <li>Press 1 for select bill pay option</li>
                                            <li>Type "1" then press "Reply".</li>
                                            <li>Enter Biller ID <b>""</b></li>
                                            <li>Enter Reference Number : <b><?php echo $_SESSION['last_id'];?></b> as your bill number</li>
                                            <li>Enter amount <b><?php echo $amount?></b></li>
                                            <li>Enter your pin number</li>
                                            <li>You will get a confirmation message.</li>
                                        </ol>
                                        <p style="color: red;font-family: 'Agency FB';font-size: 14px">NB: Please pay your registration fee for reunion-2017 within 72 hours. Date-line: <b><?php echo $newdate?></b> </p>
                                    </h5>
                                </div>
                            </div>
                        </article><!--//contact-form-->
                </div><!--//page-row-->
            </div><!--//page-content-->
        </div><!--//page-wrapper-->
    </div><!--//content-->
</div><!--//wrapper-->

<!-- ******FOOTER****** -->
<?php include("templateLayout/footer.php");?>


<?php include("templateLayout/script/templateScript.php");?>

</body>
</html>


