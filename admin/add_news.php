<?php
session_start();
require_once("../vendor/autoload.php");
include("../templateLayout/templateInformation.php");
use App\Admin;
use App\Utility\Utility;
use App\Message\Message;
if($_SESSION['role_status']==0){
    $auth= new Admin();
    $status = $auth->prepareData($_SESSION)->logged_in();

    if(!$status) {
        Utility::redirect('../login.php');
        return;
    }
}
else {
    Utility::redirect('../login.php');
}

?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<head>
    <title><?php echo $title;?></title>
    <?php include("../templateLayout/css/meta.php");?>
    <?php include("../templateLayout/css/templateCss.php");?>
    <style>
        .caption{
            text-align: center;
            font-size: 12px;
        }
        .btn{
            font-size: 11px !important;
        }
    </style>
</head>

<body class="home-page">
<div class="wrapper">
    <!-- ******HEADER****** -->
    <?php include("../templateLayout/adminNavigation.php");?>
    <!-- ******CONTENT****** -->
    <div class="content container">
        <div class="page-wrapper">
            <header class="page-heading clearfix">
                <h1 class="heading-title pull-left">Add News</h1>
                <div class="breadcrumbs pull-right">
                    <ul class="breadcrumbs-list">
                        <li class="breadcrumbs-label">You are here:</li>
                        <li><a href="#">Home</a><i class="fa fa-angle-right"></i></li>
                        <li><a href="#">Admin</a><i class="fa fa-angle-right"></i></li>
                        <li class="current">Add News</li>
                    </ul>
                </div><!--//breadcrumbs-->
            </header>

            <!--Member Panel Start-->
            <div class="row">
                <?php
                if(isset($_SESSION) && !empty($_SESSION['message'])) {

                    $msg = Message::getMessage();

                    echo "<p id='message' style='text-align: center; font-family:Century Gothic;color: red;font-size: 14px;font-weight: 600;'>$msg</p>";

                }

                ?>
                <div class="col-md-12 ">


                            <?php
                            if(!isset($_FILES['title_pic']['name'])){
                                    ?>
                                        <div class="col-md-6 col-sm-6 col-md-offset-3 col-sm-offset-3 col-xs-12">
                                            <form action="add_news.php" method="post" enctype="multipart/form-data">
                                                <div class="form-group">
                                                    <label>টাইটেল পিকচারঃ (Picture size must be 300px*300px)</label>
                                                    <input type="file" class="form-control" name="title_pic"  required>
                                                </div><!--//form-group-->
                                                <input type="hidden" class="form-control" name="mod_email" value="<?php echo $_SESSION['email']?>">
                                                <div class="form-group">
                                                    <label>নিউজ কভার ফটোঃ (Picture size must be 700px*300px)</label>
                                                    <input type="file" class="form-control" name="cover_pic" required>
                                                </div><!--//form-group-->
                                                <input type="submit" class="btn btn-primary" value="Upload picture">
                                                <br><br>
                                            </form>
                                        </div>
                                    <?php
                                }
                            ?>
                            <?php
                            if(isset($_FILES) && !empty($_FILES)){
                                $titlePicName=time().$_FILES['title_pic']['name'];
                                $coverPicName=time().$_FILES['cover_pic']['name'];
                                $titlePicTemp=$_FILES['title_pic']['tmp_name'];
                                $coverPicTemp=$_FILES['cover_pic']['tmp_name'];
                                $titlePicSize=getimagesize($titlePicTemp);
                                $titlePicWidth=$titlePicSize[0];
                                $titlePicHeight=$titlePicSize[1];
                                $coverPicSize=getimagesize($coverPicTemp);
                                $coverPicWidth=$coverPicSize[0];
                                $coverPicHeight=$coverPicSize[1];

                                if($titlePicWidth==300 && $titlePicHeight==300 && $coverPicWidth==700 && $coverPicHeight==300){
                                    $_POST['title_pic']=$titlePicName;
                                    $_POST['cover_pic']=$coverPicName;
                                    move_uploaded_file($titlePicTemp,'../resources/news/title/'.$titlePicName);
                                    move_uploaded_file($coverPicTemp,'../resources/news/cover/'.$coverPicName);
                                }
                                else{
                                    ?>
                                    <div style="background: #080058;">
                                        <a href="add_news.php" style=""><p style="color: red;overflow: hidden;text-align:center;line-height: 25px;font-size: 12px;">Image size doesn't match! Try again!</p></a>
                                    </div>
                                    <?php
                                }
                            }
                            ?>
                            <?php
                            if(isset($_POST) && !empty($_POST)){
                                ?>
                                <div class="col-md-6 col-sm-6 col-md-offset-3 col-sm-offset-3 col-xs-12">
                                    <div class="form-group">
                                        <label>টাইটেল পিকচারঃ</label>
                                        <img src="../resources/news/title/<?php echo $_POST['title_pic']?>" class="img-responsive img-rounded" width="25%"/>
                                    </div><!--//form-group-->
                                    <div class="form-group">
                                        <label>নিউজ কভার ফটোঃ</label>
                                        <img src="../resources/news/cover/<?php echo $_POST['cover_pic']?>" class="img-responsive img-rounded" width="80%"/>
                                    </div><!--//form-group-->
                                    <div style="background: #080058;width: 80%">
                                        <a href="add_news.php" style=""><p style="color: red;overflow: hidden;text-align:center;line-height: 25px;font-size: 12px;">Delete picture!</p></a>
                                    </div>
                                    <form action="../controller/newsProcess.php" method="post" enctype="multipart/form-data">
                                        <div class="form-group">
                                            <label>নিউজ হেডলাইন লিখুন</label>
                                            <input type="text" class="form-control" name="headline" placeholder="নিউজ হেডলাইন লিখুন" required>
                                        </div><!--//form-group-->
                                        <div class="form-group">
                                            <label>বিস্তারিত সংবাদ লিখুন<span class="required">*</span></label>
                                            <textarea id="text" class="form-control" rows="16" name="details" placeholder="বিস্তারিত লিখুন" required></textarea>
                                        </div><!--//form-group-->
                                        <input type="hidden" name="title_pic" value="<?php echo $_POST['title_pic']?>">
                                        <input type="hidden" name="cover_pic" value="<?php echo $_POST['cover_pic']?>">
                                        <input type="hidden" name="admin_email" value="<?php echo $_SESSION['email']?>">
                                        <div class="form-group">
                                            <input type="submit" class="btn btn-primary" <?php if(!isset($_FILES['title_pic']['name'])){echo "value='Upload picture first'";} else{echo "value='Add News'";}?> <?php if(!isset($_FILES['title_pic']['name'])){echo "disabled";}?>>
                                        </div><!--//form-group-->
                                    </form>
                                </div>

                                <?php
                            }?>
                        </div>




                </div>
            </div>
            <!--Member Panel End-->
            <hr>

        </div>
    </div><!--//content-->
</div><!--//wrapper-->

<!-- ******FOOTER****** -->
<?php include("../templateLayout/footer.php");?>


<?php include("../templateLayout/script/templateScript.php");?>
<script src="//cdn.ckeditor.com/4.5.5/standard/ckeditor.js"></script>
<script> CKEDITOR.replace( 'text' );</script>
<script> CKEDITOR.replace( 'text2' );</script>
</body>
</html>

