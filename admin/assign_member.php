<?php
include("../templateLayout/templateInformation.php");
include_once "../vendor/autoload.php";
session_start();
use App\Admin;
use App\Utility\Utility;
use App\Message\Message;
if($_SESSION['role_status']==0){
    $auth= new Admin();
    $status = $auth->prepareData($_SESSION)->logged_in();

    if(!$status) {
        Utility::redirect('../login.php');
        return;
    }
}
else {
    Utility::redirect('../login.php');
}
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<head>
    <title><?php echo $title;?></title>
    <?php include("../templateLayout/css/meta.php");?>
    <?php include("../templateLayout/css/templateCss.php");?>
    <script src="http://demo.itsolutionstuff.com/plugin/jquery.js"></script>
    <script src="http://demo.itsolutionstuff.com/plugin/croppie.js"></script>
    <link rel="stylesheet" href="http://demo.itsolutionstuff.com/plugin/croppie.css">

</head>

<body class="home-page">
<div class="wrapper">
    <!-- ******HEADER****** -->
    <?php include("../templateLayout/adminNavigation.php");?>
    <!-- ******CONTENT****** -->
    <div class="content container">
        <div class="page-wrapper">
            <header class="page-heading clearfix">
                <h1 class="heading-title pull-left">Assign Member</h1>
                <div class="breadcrumbs pull-right">
                    <ul class="breadcrumbs-list">
                        <li class="breadcrumbs-label">You are here:</li>
                        <li><a href="admin_profile.php">Home</a><i class="fa fa-angle-right"></i></li>
                        <li><a href="#">Admin</a><i class="fa fa-angle-right"></i></li>
                        <li class="current">Assign Member</li>
                    </ul>
                </div><!--//breadcrumbs-->
            </header>
            <div class="page-content">
                <div class="row">
                    <?php
                    if(isset($_SESSION) && !empty($_SESSION['message'])) {

                        $msg = Message::getMessage();

                        echo "
                        <p id='message' style='text-align: center; font-family:Century Gothic;color: red;font-size: 14px;font-weight: 600;'>$msg</p>";

                    }

                    ?>
                    <div class="col-md-8 col-sm-8 col-md-offset-2 col-sm-offset-2 col-xs-12 col-xs-offset-0">
                        <article class="contact-form col-md-12 col-sm-12 col-xs-12  page-row">
                            <?php
                            if(isset($_POST['pic']) & !empty($_POST['pic'])){
                                ?>
                                <div class="row">
                                    <div class="col-md-4 col-sm-4 col-xs-6">
                                        <img src="../resources/members_photo/<?php echo $_POST['pic']?>" class="img-responsive" width="100%">
                                    </div>
                                </div>

                                <form action="../controller/assign_member.php" method="post">
                                    <h5 style="color: #2e6da4"><b>Personal Information</b></h5>
                                    <input type="hidden" name="picture" value="<?php echo $_POST['pic']?>">
                                    <div class="form-group name">
                                        <label for="name">Name </label>
                                        <input id="name" type="text" class="form-control" placeholder="Enter your name" name="member_name"  required>
                                    </div><!--//form-group-->
                                    <div class="form-group name">
                                        <label for="name">Mother's Name </label>
                                        <input id="name" type="text" class="form-control" placeholder="Enter your mother's name" name="mother_name"  required>
                                    </div><!--//form-group-->
                                    <div class="form-group name">
                                        <label for="name">Father's Name </label>
                                        <input id="name" type="text" class="form-control" placeholder="Enter your father's name" name="father_name"  required>
                                    </div><!--//form-group-->
                                    <div class="form-group name">
                                        <label for="name">Spouse Name </label>
                                        <input id="name" type="text" class="form-control" placeholder="Enter your spouse name" name="spouse_name" >
                                    </div><!--//form-group-->
                                    <div class="row">
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="row">
                                                <div class="col-md-12 cool-sm-12 col-xs-12">
                                                    <label for="name">Date of Birth</label>
                                                    <div class="row">
                                                        <div class="col-md-4 col-sm-4 col-xs-4">
                                                            <div class="form-group name">
                                                                <input id="name" type="number" class="form-control" placeholder="D" min="1" max="31" name="date"  required>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4 col-sm-4 col-xs-4 ">
                                                            <div class="form-group name">
                                                                <input id="name" type="number" class="form-control" placeholder="M" min="1" max="12" name="month"  required>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4 col-sm-4 col-xs-4">
                                                            <div class="form-group name">
                                                                <input id="name" type="number" class="form-control" placeholder="Y" name="year"  required>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group name">
                                                <label for="name">Blood Group</label>
                                                <select class="form-control" name="blood_group" >
                                                    <option value="Undefined">Select Your Blood Group</option>
                                                    <option value="A+">A+</option>
                                                    <option value="A-">A-</option>
                                                    <option value="B+">B+</option>
                                                    <option value="B-">B-</option>
                                                    <option value="O+">O+</option>
                                                    <option value="O-">O-</option>
                                                    <option value="AB+">AB+</option>
                                                    <option value="AB-">AB-</option>
                                                </select>
                                            </div><!--//form-group-->

                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group name">
                                                <label for="name">NID No</label>
                                                <input id="name" type="number" class="form-control" placeholder="Enter your NID no" name="nid_no" >
                                            </div><!--//form-group-->
                                        </div>
                                        <div class="col-md-3 col-sm-3 col-xs-6">
                                            <div class="form-group name">
                                                <label for="name">Gender</label>
                                                <select class="form-control" name="gender"  required>
                                                    <option value="Male">Male</option>
                                                    <option value="Female">Female</option>
                                                    <option value="Undefined">Undefined</option>

                                                </select>
                                            </div><!--//form-group-->
                                        </div>
                                        <div class="col-md-3 col-sm-3 col-xs-6">
                                            <div class="form-group name">
                                                <label for="name">Marital Status</label>
                                                <select class="form-control" name="marital" required>
                                                    <option value="Single">Single</option>
                                                    <option value="Married">Married</option>
                                                    <option value="Divorced">Divorced</option>
                                                    <option value="Widow">Widow</option>
                                                </select>
                                            </div><!--//form-group-->
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <label for="name">Permanent Address</label>
                                        </div>

                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                            <div class="form-group name">
                                                <label for="name">Vill</label>
                                                <input id="name" type="text" class="form-control" placeholder="Village" name="vill"  required>
                                            </div><!--//form-group-->

                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                            <div class="form-group name">
                                                <label for="name">Post Office</label>
                                                <input id="name" type="text" class="form-control" placeholder="Post office" name="post_office"  required>
                                            </div><!--//form-group-->
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                            <div class="form-group name">
                                                <label for="name">Police Station</label>
                                                <input id="name" type="text" class="form-control" placeholder="Police Station" name="police_station"  required>
                                            </div><!--//form-group-->
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                            <div class="form-group name">
                                                <label for="name">District</label>
                                                <input id="name" type="text" class="form-control" placeholder="District" name="district"  required>
                                            </div><!--//form-group-->
                                        </div>
                                    </div>
                                    <div class="form-group name">
                                        <label for="name">Present Address</label>
                                        <input id="name" type="text" class="form-control" placeholder="Enter your present address" name="present_address"  required>
                                    </div><!--//form-group-->

                                    <div class="row">

                                        <div class="col-md-4 col-sm-4 col-xs-6">
                                            <div class="form-group name">
                                                <label for="name">Mobile No</label>
                                                <input id="name" type="number" minlength="11" class="form-control" placeholder="Enter your contact no" name="contact"  required>
                                            </div><!--//form-group-->
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-6">
                                            <div class="form-group name">
                                                <label for="name">Email</label>
                                                <input id="name" type="email" class="form-control" placeholder="Enter your email" name="email" >
                                            </div><!--//form-group-->
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-12">
                                            <div class="form-group name">
                                                <label for="name">FB ID</label>
                                                <input id="name" type="text" class="form-control" placeholder="Enter your facebook username" name="fb_id" >
                                            </div><!--//form-group-->
                                        </div>
                                    </div>

                                    <div class="row">

                                        <div class="col-md-4 col-sm-4 col-xs-12">
                                            <div class="form-group name">
                                                <label for="name">Occupation</label>
                                                <input id="name" type="text" class="form-control" placeholder="Enter your occupation" name="occupation" >
                                            </div><!--//form-group-->
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-12">
                                            <div class="form-group name">
                                                <label for="name">Organization</label>
                                                <input id="name" type="text" class="form-control" placeholder="Enter your organization" name="organization" >
                                            </div><!--//form-group-->
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-12">
                                            <div class="form-group name">
                                                <label for="name">Designation</label>
                                                <input id="name" type="text" class="form-control" placeholder="Enter your designation" name="designation">
                                            </div><!--//form-group-->
                                        </div>
                                    </div>
                                    <h5 style="color: #2e6da4"><br><br><b>School Information</b></h5>
                                    <div class="row">
                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                            <div class="form-group name">
                                                <label for="name">The class you admitted</label>
                                                <select class="form-control" name="admitted_class"  required>
                                                    <option value="Six">Six</option>
                                                    <option value="Seven">Seven</option>
                                                    <option value="Eight">Eight</option>
                                                    <option value="Nine">Nine</option>
                                                    <option value="Ten">Ten</option>
                                                </select>
                                            </div><!--//form-group-->
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                            <div class="form-group name">
                                                <label for="name">In which class you left</label>
                                                <select class="form-control" name="left_class"  required>
                                                    <option value="Six">Six</option>
                                                    <option value="Seven">Seven</option>
                                                    <option value="Eight">Eight</option>
                                                    <option value="Nine">Nine</option>
                                                    <option value="Ten">Ten</option>
                                                </select>
                                            </div><!--//form-group-->
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group name">
                                                <label for="name">Have you passed from the school?</label><br>
                                                <select class="form-control" name="passed_from_school"  required>
                                                    <option value="Yes">Yes</option>
                                                    <option value="No">No</option>
                                                </select>
                                            </div><!--//form-group-->
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group name">
                                                <label for="name">Passing Year</label>
                                                <select class="form-control" name="passing_year"  required>
                                                    <option value="9999">Regular</option>
                                                    <option value="2002">2002</option>
                                                    <option value="2003">2003</option>
                                                    <option value="2004">2004</option>
                                                    <option value="2005">2005</option>
                                                    <option value="2006">2006</option>
                                                    <option value="2007">2007</option>
                                                    <option value="2008">2008</option>
                                                    <option value="2009">2009</option>
                                                    <option value="2010">2010</option>
                                                    <option value="2011">2011</option>
                                                    <option value="2012">2012</option>
                                                    <option value="2013">2013</option>
                                                    <option value="2014">2014</option>
                                                    <option value="2015">2015</option>
                                                    <option value="2016">2016</option>
                                                    <option value="2017">2017</option>
                                                </select>
                                            </div><!--//form-group-->
                                        </div>
                                    </div>
                                    <h5 style="color: #2e6da4"><br><br><b>Batch Mate (For recognize)</b></h5>
                                    <div class="row">
                                        <div class="col-md-4 col-sm-4 col-xs-12">
                                            <div class="form-group name">
                                                <label for="name">Name (Person 1)</label>
                                                <input id="name" type="text" class="form-control" placeholder="Batch mate's name" name="batch_mate[]"  required>
                                            </div><!--//form-group-->
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-6">
                                            <div class="form-group name">
                                                <label for="name">Mobile No</label>
                                                <input id="name" type="number" minlength="11" class="form-control" placeholder="Batch mate's contact no" name="batch_mate_contact[]"  required>
                                            </div><!--//form-group-->
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-6">
                                            <div class="form-group name">
                                                <label for="name">FB ID</label>
                                                <input id="name" type="text" class="form-control" placeholder="Batch mate's facebook username" name="batch_mate_fb[]"  required>
                                            </div><!--//form-group-->
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-12">
                                            <div class="form-group name">
                                                <label for="name">Name (Person 2)</label>
                                                <input id="name" type="text" class="form-control" placeholder="Batch mate's name" name="batch_mate[]"  required>
                                            </div><!--//form-group-->
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-6">
                                            <div class="form-group name">
                                                <label for="name">Mobile No</label>
                                                <input id="name" type="number" minlength="11" class="form-control" placeholder="Batch mate's contact no" name="batch_mate_contact[]"  required>
                                            </div><!--//form-group-->
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-6">
                                            <div class="form-group name">
                                                <label for="name">FB ID</label>
                                                <input id="name" type="text" class="form-control" placeholder="Batch mate's facebook username" name="batch_mate_fb[]"  required>
                                            </div><!--//form-group-->
                                        </div>

                                    </div>
                                    <div class="form-group name">

                                        <input id="name" type="checkbox" required> <p>I hereby declare that the particulars provided are true to the best of my knowledge and i will
                                            obey all rules according to <b>Alumni Constitution</b>. I authorize you to verify all information contained in this application.
                                            and take any action against me, if there any rules violation exists.</p>
                                    </div><!--//form-group-->

                                    <button type="submit" class="btn btn-theme" >Next Step</button>
                                </form>
                                <?php
                            }
                            else{
                                ?>
                                <div class="row">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">Image Upload</div>
                                        <div class="panel-body">

                                            <div class="row">
                                                <div class="col-md-12 col-xs-12 col-sm-12 text-center">
                                                    <strong>Select Image:</strong>
                                                    <br/>
                                                    <div class="form-group">
                                                        <input type="file" class="form-control" id="upload">
                                                    </div>
                                                    <br/>
                                                </div>
                                                <div class="col-md-6 col-sm-6 col-xs-12 text-center">
                                                    <div id="upload-demo" style="width:100%;margin: 0 auto"></div>
                                                    <button class="btn btn-success upload-result">Upload Image</button>
                                                </div>
                                                <div class="col-md-6 col-sm-6 col-xs-12 text-center">
                                                    <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: 35px">
                                                        <div id="upload-demo-i" style="background:#e1e1e1;width:100%;padding:0px;min-height:300px;margin: 0 auto;"></div>
                                                    </div>
                                                </div>


                                            </div>

                                        </div>
                                        <div class="panel-footer">
                                            <form action="assign_member.php" method="post">
                                                <div id="upload-demo-i2"></div>
                                                <input type="submit" class="btn btn-primary" value="Next Step">
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>
                        </article><!--//contact-form-->
                    </div>

                </div><!--//page-row-->
            </div><!--//page-content-->
        </div><!--//page-wrapper-->
    </div><!--//content-->
</div><!--//wrapper-->

<!-- ******FOOTER****** -->
<?php include("../templateLayout/footer.php");?>


<script type="text/javascript">
    $uploadCrop = $('#upload-demo').croppie({
        enableExif: true,
        viewport: {
            width: 300,
            height: 300,
            type: 'rectangle'
        },
        boundary: {
            width: 320,
            height: 320
        }
    });

    $('#upload').on('change', function () {
        var reader = new FileReader();
        reader.onload = function (e) {
            $uploadCrop.croppie('bind', {
                url: e.target.result
            }).then(function(){
                console.log('jQuery bind complete');
            });

        }
        reader.readAsDataURL(this.files[0]);
    });

    $('.upload-result').on('click', function (ev) {
        $uploadCrop.croppie('result', {
            type: 'canvas',
            size: 'viewport'
        }).then(function (resp) {

            $.ajax({
                url: "ajaxpro.php",
                type: "POST",
                data: {"image":resp},
                dataType: 'json',
                success: function (data) {
                    html = '<img src="' + resp + '" class="img-responsive" width="100%"/>';
                    html2 = '<input type="hidden" name="pic" value="' + data + '" required/>';
                    $("#upload-demo-i").html(html);
                    $("#upload-demo-i2").html(html2);

                }
            });
        });
    });

</script>
<?php include("../templateLayout/script/templateScript.php");?>

</body>
</html>

