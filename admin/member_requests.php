<?php
require_once("../vendor/autoload.php");
include ("../templateLayout/templateInformation.php");
session_start();
use App\Admin;
use App\Utility\Utility;
use App\Message\Message;
if($_SESSION['role_status']==0){
    $auth= new Admin();
    $status = $auth->prepareData($_SESSION)->logged_in();

    if(!$status) {
        Utility::redirect('../login.php');
        return;
    }
}
else {
    Utility::redirect('../login.php');
}
use App\Members;
$object=new Members();
$memberData=$object->showRequest();

?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<head>
    <title><?php echo $title;?></title>
    <?php include("../templateLayout/css/meta.php");?>
    <?php include ("../templateLayout/css/templateCss.php");?>
    <?php include('../templateLayout/css/tableCss.php');?>
</head>

<body class="home-page">
<div class="wrapper">
    <!-- ******HEADER****** -->
    <?php include ("../templateLayout/adminNavigation.php");?>
    <!-- ******CONTENT****** -->
    <div class="content container">
        <div class="page-wrapper">
            <header class="page-heading clearfix">
                <h1 class="heading-title pull-left">Member Request</h1>
                <div class="breadcrumbs pull-right">
                    <ul class="breadcrumbs-list">
                        <li class="breadcrumbs-label">You are here:</li>
                        <li><a href="#">Home</a><i class="fa fa-angle-right"></i></li>
                        <li><a href="#">Admin</a><i class="fa fa-angle-right"></i></li>
                        <li class="current">Member Request</li>
                    </ul>
                </div><!--//breadcrumbs-->
            </header>
        <!--Member Panel Start-->
        <div class="row">
            <?php
            if(isset($_SESSION) && !empty($_SESSION['message'])) {

                $msg = Message::getMessage();

                echo "<p id='message' style='text-align: center; font-family:Century Gothic;color: red;font-size: 14px;font-weight: 600;'>$msg</p>";

            }

            ?>

            <div class="col-md-12 ">
                <?php
                foreach ($memberData as $oneData){
                    $name=substr($oneData->name,0,15);
                    ?>
                    <div class="col-md-2 col-sm-3 col-xs-6">
                        <div class="thumbnail">
                            <div>
                                <img src="../resources/members_photo/<?php echo $oneData->picture?>" class="img-responsive img-rounded" >
                                <p class="caption" style="text-align: center"><?php echo $name?></p>
                            </div>
                            <a href='memberProfile.php?member_id=<?php echo $oneData->id?>' class="btn btn-primary" style="width: 100%">See Profile</a>
                        </div>
                    </div>
                    <?php
                }
                ?>


            </div>
        </div>
        <!--Member Panel End-->
        <hr>
        </div>
    </div><!--//content-->
</div><!--//wrapper-->
<?php include ("../templateLayout/footer.php");?>


<?php include ("../templateLayout/script/templateScript.php");?>
<?php include('../templateLayout/script/tableScript.php');?>
</body>
</html>

