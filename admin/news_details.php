<?php
session_start();
require_once ("../vendor/autoload.php");
include ("../templateLayout/templateInformation.php");
use App\Admin;
use App\Utility\Utility;
use App\Message\Message;
if($_SESSION['role_status']==0){
    $auth= new Admin();
    $status = $auth->prepareData($_SESSION)->logged_in();

    if(!$status) {
        Utility::redirect('../login.php');
        return;
    }
}
else {
    Utility::redirect('../login.php');
}
use App\News;
$object=new News();
$object->prepareData($_GET);
$onenewsData =$object->showSingleData();
$sideNews=$object->showallNewsSidebar();

$date = date("d", strtotime("$onenewsData->date"));
$month = date("m", strtotime("$onenewsData->date"));
$year = date("Y", strtotime("$onenewsData->date"));
if($month=='01'){
    $monthD='January';
}
if($month=='02'){
    $monthD='February';
}
else if($month=='03'){
    $monthD='March';
}
else if($month=='04'){
    $monthD='April';
}
else if($month=='05'){
    $monthD='May';
}
else if($month=='06'){
    $monthD='June';
}
else if($month=='07'){
    $monthD='July';
}
else if($month=='08'){
    $monthD='August';
}
else if($month=='09'){
    $monthD='September';
}
else if($month=='10'){
    $monthD='October';
}
else if($month=='11'){
    $monthD='November';
}
else if($month=='12'){
    $monthD='December';
}
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<head>
    <title><?php echo $onenewsData->headline;?></title>
    <?php include("../templateLayout/css/meta.php");?>
    <?php include ("../templateLayout/css/templateCss.php");?>

</head>

<body class="home-page">
<div class="wrapper">
    <!-- ******HEADER****** -->
    <?php include ("../templateLayout/adminNavigation.php");?>
    <!-- ******CONTENT****** -->
    <div class="content container">
        <div class="page-wrapper">
            <header class="page-heading clearfix">
                <h1 class="heading-title pull-left"><?php echo $onenewsData->headline?></h1>
                <div class="breadcrumbs pull-right">
                    <ul class="breadcrumbs-list">
                        <li class="breadcrumbs-label">You are here:</li>
                        <li><a href="">Home</a><i class="fa fa-angle-right"></i></li>
                        <li><a href="">News</a><i class="fa fa-angle-right"></i></li>
                        <li class="current"><?php echo $onenewsData->headline?></li>
                    </ul>
                </div><!--//breadcrumbs-->
            </header>
            <div class="page-content">
                <div class="row page-row">
                    <div class="news-wrapper col-md-8 col-sm-7">
                        <article class="news-item">
                            <?php
                            if(isset($_SESSION) && !empty($_SESSION['message'])) {

                                $msg = Message::getMessage();

                                echo "
                        <p id='message' style='text-align: center; font-family: Pristina; color: red; font-size: 25px'>$msg</p>";

                            }

                            ?>
                            <p class="meta text-muted">By: <a href="#"><?php echo $onenewsData->admin_name?></a> | Posted on: <?php echo $date;if($date==01){echo "<sup>st</sup>";} elseif($date==31){echo "<sup>st</sup>";} elseif($date==2){echo "<sup>nd</sup>";}elseif($date==3){echo "<sup>rd</sup>";} else{echo "<sup>th</sup>";}?> <?php echo " ".$monthD." ".$year ?></p>

                            <?php
                                if($onenewsData->status==0){
                                   ?>
                                    <div class="dropdown">
                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">Re-store This News</button>
                                        <div class="modal fade" id="myModal" role="dialog">
                                            <div class="modal-dialog">

                                                <!-- Modal content-->
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title"><?php echo $onenewsData->headline?></h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <h5>Are You Sure to re-store this news?</h5>
                                                        <a href="../controller/restoreNewsAdmin.php?news_id=<?php echo $onenewsData->id?>" class="btn btn-primary">Yes</a>
                                                        <a href="" data-dismiss="modal" class="btn btn-danger">No</a>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                    </div>
                            <?php
                                }
                            else if($onenewsData->status==1){
                                ?>
                            <div class="dropdown">
                                <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Edit News
                                    <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a href="editTitlePic.php?news_id=<?php echo $onenewsData->id?>">Change Title Picture</a></li>
                                    <li><a href="editCoverPic.php?news_id=<?php echo $onenewsData->id?>" >Change Cover photo</a></li>
                                    <li><a href="editNews.php?news_id=<?php echo $onenewsData->id?>">Edit News Content</a></li>
                                </ul>
                                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModal">Delete This News</button>
                                <div class="modal fade" id="myModal" role="dialog">
                                    <div class="modal-dialog">

                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title"><?php echo $onenewsData->headline?></h4>
                                            </div>
                                            <div class="modal-body">
                                                <h5>Are You Sure to delete this news?</h5>
                                                <a href="../controller/deleteNewsAdmin.php?news_id=<?php echo $onenewsData->id?>" class="btn btn-primary">Yes</a>
                                                <a href="" data-dismiss="modal" class="btn btn-danger">No</a>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </div>
                            <?php
                            }
                            ?>
                                <p></p>


                            <p class="featured-image"><img class="img-responsive" src="../resources/news/cover/<?php echo $onenewsData->cover_pic?>" alt=""  width="100%"/></p>
                            <p><?php echo $onenewsData->news?></p>

                        </article><!--//news-item-->
                    </div><!--//news-wrapper-->
                    <aside class="page-sidebar  col-md-3 col-md-offset-1 col-sm-4 col-sm-offset-1 col-xs-12">
                        <section class="widget has-divider">

                                   <br> <h3 class="title">Other News</h3>
                                    <?php
                                    foreach ($sideNews as $oneNews){
                                        ?>
                                        <article class="news-item">
                                            <div class="row">
                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                    <div class="col-md-2 col-sm-3 col-xs-3">
                                                        <figure class="thumb">
                                                            <img src="../resources/news/title/<?php echo $oneNews->title_pic?>" alt="" width="100%"/>
                                                        </figure>
                                                    </div>
                                                    <div class=" col-md-10 col-sm-9 col-xs-9">
                                                        <div class="details">
                                                            <h4 style="font-size: 12px"><a href="news_details.php?news_id=<?php echo $oneNews->id?>"><?php echo $oneNews->headline?></a></h4>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                </div>
                                            </div>


                                        </article><!--//news-item-->
                                        <hr>
                                        <?php
                                    }
                                    ?>
                        </section><!--//widget-->
                    </aside>
                </div><!--//page-row-->
            </div><!--//page-content-->
        </div><!--//page-->
    </div><!--//content-->
</div><!--//wrapper-->

<!-- ******FOOTER****** -->
<?php include ("../templateLayout/footer.php");?>


<?php include ("../templateLayout/script/templateScript.php");?>

</body>
</html> 

