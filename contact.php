<?php include("templateLayout/templateInformation.php");

?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<head>
    <title><?php echo $title;?></title>
    <?php include("templateLayout/css/meta.php");?>
    <?php include("templateLayout/css/templateCss.php");?>

</head>

<body class="home-page">
<div class="wrapper">
    <!-- ******HEADER****** -->
    <?php include("templateLayout/headerAndNavigation.php");?>

    <!-- ******CONTENT****** -->
    <div class="content container">
        <div class="page-wrapper">
            <header class="page-heading clearfix">
                <h1 class="heading-title pull-left">Contact</h1>
                <div class="breadcrumbs pull-right">
                    <ul class="breadcrumbs-list">
                        <li class="breadcrumbs-label">You are here:</li>
                        <li><a href="index.php">Home</a><i class="fa fa-angle-right"></i></li>
                        <li class="current">Contact</li>
                    </ul>
                </div><!--//breadcrumbs-->
            </header>
            <div class="page-content">
                <div class="row">
                    <article class="contact-form col-md-8 col-sm-7  page-row">
                        <h3 class="title">Get in touch</h3>
                        <p>আমাদের সাথে যোগাযোগ করতে অথবা আমাদের কর্মকান্ড নিয়ে কোন মতামত থাকলে আমাদের মেইল করুন।</p>
                        <form>
                            <div class="form-group name">
                                <label for="name">Name</label>
                                <input id="name" type="text" class="form-control" placeholder="Enter your name">
                            </div><!--//form-group-->
                            <div class="form-group email">
                                <label for="email">Email<span class="required">*</span></label>
                                <input id="email" type="email" class="form-control" placeholder="Enter your email">
                            </div><!--//form-group-->
                            <div class="form-group phone">
                                <label for="phone">Phone</label>
                                <input id="phone" type="tel" class="form-control" placeholder="Enter your contact number">
                            </div><!--//form-group-->
                            <div class="form-group message">
                                <label for="message">Message<span class="required">*</span></label>
                                <textarea class="form-control" rows="6" placeholder="Enter your message here..."></textarea>
                            </div><!--//form-group-->
                            <button type="submit" class="btn btn-theme">Send message</button>
                        </form>
                    </article><!--//contact-form-->
                    <aside class="page-sidebar  col-md-3 col-md-offset-1 col-sm-4 col-sm-offset-1">


                        <section class="widget has-divider">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <h3 class="title"><br>Postal Address</h3>
                                <p class="adr">
                                    <span class="adr-group">
                                        <span class="street-address"><i class="fa fa-map-marker pull-left"></i> Chittagong Ideal High School</span><br>
                                        <span class="postal-code"><i class="fa fa-map-marker pull-left"></i> Harinkhain, Patiya</span><br>
                                        <span class="country-name"><i class="fa fa-map-marker pull-left"></i> Chittagong</span>
                                    </span>
                                </p>
                            </div>


                        </section><!--//widget-->

                        <section class="widget">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <h3 class="title"><br>All Enquiries</h3>
                                <p class="tel col-md-12 col-sm-4"><i class="fa fa-phone"></i>01826-132308</p>
                                <p class="email col-md-12 col-sm-4"><i class="fa fa-envelope"></i><a href="#">info@cihsalumni.org</a></p>
                            </div>

                        </section>
                    </aside><!--//page-sidebar-->
                </div><!--//page-row-->
            </div><!--//page-content-->
        </div><!--//page-wrapper-->
    </div><!--//content-->
</div><!--//wrapper-->

<!-- ******FOOTER****** -->
<?php include("templateLayout/footer.php");?>


<?php include("templateLayout/script/templateScript.php");?>

</body>
</html> 

